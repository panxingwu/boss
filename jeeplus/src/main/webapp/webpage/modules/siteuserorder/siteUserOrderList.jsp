<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>分期订单管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			laydate({
	            elem: '#checkDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>分期订单管理列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="siteUserOrder" action="${ctx}/siteuserorder/siteUserOrder/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>订单号：</span>
				<form:input path="id" htmlEscape="false" maxlength="64"  class=" form-control input-sm"/>
			<span>租户姓名：</span>
				<form:input path="tenancyName" htmlEscape="false" maxlength="50"  class=" form-control input-sm"/>
			<span>租户手机号：</span>
				<form:input path="tenancyMobile" htmlEscape="false" maxlength="15"  class=" form-control input-sm"/>
			<span>审核日期：</span>
				<input id="checkDate" name="checkDate" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
					value="<fmt:formatDate value="${siteUserOrder.checkDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
			<span>中介公司：</span>
				<form:input path="companName" htmlEscape="false" maxlength="100"  class=" form-control input-sm"/>
			<span>订单状态：</span>
				<form:select path="status"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('siteuser_order_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="siteuserorder:siteUserOrder:add">
				<table:addRow url="${ctx}/siteuserorder/siteUserOrder/form" title="分期订单管理"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="siteuserorder:siteUserOrder:edit">
			    <table:editRow url="${ctx}/siteuserorder/siteUserOrder/form" title="分期订单管理" id="contentTable"></table:editRow><!-- 编辑按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="siteuserorder:siteUserOrder:del">
				<table:delRow url="${ctx}/siteuserorder/siteUserOrder/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="siteuserorder:siteUserOrder:import">
				<table:importExcel url="${ctx}/siteuserorder/siteUserOrder/import"></table:importExcel><!-- 导入按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="siteuserorder:siteUserOrder:export">
	       		<table:exportExcel url="${ctx}/siteuserorder/siteUserOrder/export"></table:exportExcel><!-- 导出按钮 -->
	       	</shiro:hasPermission>
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column id">订单号</th>
				<th  class="sort-column platformPayType">垫付方式</th>
				<th  class="sort-column status">状态</th>
				<th  class="sort-column companName">中介公司</th>
				<th  class="sort-column cash">月租金</th>
				<th  class="sort-column cash">月手续费</th>
				<th  class="sort-column cash">月还款额</th>
				<th  class="sort-column tenancyName">租户姓名</th>
				<th  class="sort-column startTime">起租日</th>
				<th  class="sort-column endTime">到期日</th>
				<th  class="sort-column gmtCreate">申请时间</th>
				<th  class="sort-column checkDate">审核时间</th>
				<th  class="sort-column checkUserId">审核人</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="siteUserOrder">
			<tr>
				<td> <input type="checkbox" id="${siteUserOrder.id}" class="i-checks"></td>
				<td><a  href="#" onclick="openDialogView('查看分期订单管理', '${ctx}/siteuserorder/siteUserOrder/form?id=${siteUserOrder.id}','800px', '500px')">
					${siteUserOrder.id}
				</a></td>
				<td>
					<c:if test="${siteUserOrder.platformPayType==1}">季付</c:if>
					<c:if test="${siteUserOrder.platformPayType==2}">半年付</c:if>
					<c:if test="${siteUserOrder.platformPayType==3}">年付</c:if>
				</td>
				<td>
					${fns:getDictLabel(siteUserOrder.status, 'siteuser_order_status', '')}
				</td>
				<td>
					${siteUserOrder.companName}
				</td>				
				<td>
					${siteUserOrder.cash/100}
				</td>
				<td>
					<fmt:formatNumber type="number" value="${siteUserOrder.cash*siteUserOrder.rate/100}" pattern="0.00" maxFractionDigits="2"/> 
				</td>
				<td>
					<fmt:formatNumber type="number" value="${siteUserOrder.cash/100+siteUserOrder.cash*siteUserOrder.rate/100}" pattern="0.00" maxFractionDigits="2"/> 
				</td>
				<td>
					${siteUserOrder.tenancyName}
				</td>
				<td>
					<fmt:formatDate value="${siteUserOrder.startTime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${siteUserOrder.endTime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${siteUserOrder.gmtCreate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${siteUserOrder.checkDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${siteUserOrder.checkUserId}
				</td>
				<td>
					<shiro:hasPermission name="siteuserorder:siteUserOrder:view">
						<a href="#" onclick="openDialogView('查看分期订单管理', '${ctx}/siteuserorder/siteUserOrder/form?id=${siteUserOrder.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
					</shiro:hasPermission>
					<shiro:hasPermission name="siteuserorder:siteUserOrder:edit">
    					<a href="#" onclick="openDialog('修改分期订单管理', '${ctx}/siteuserorder/siteUserOrder/form?id=${siteUserOrder.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
    				<shiro:hasPermission name="siteuserorder:siteUserOrder:del">
						<a href="${ctx}/siteuserorder/siteUserOrder/delete?id=${siteUserOrder.id}" onclick="return confirmx('确认要删除该分期订单管理吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>