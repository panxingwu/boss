<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>分期订单管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#checkDate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="siteUserOrder" action="${ctx}/siteuserorder/siteUserOrder/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">租户支付订单号：</label></td>
					<td class="width-35">
						<form:input path="orderNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">经纪人ID：</label></td>
					<td class="width-35">
						<sys:treeselect id="user" name="user.id" value="${siteUserOrder.user.id}" labelName="user.name" labelValue="${siteUserOrder.user.name}"
							title="用户" url="/sys/office/treeData?type=3" cssClass="form-control " allowClear="true" notAllowSelectParent="true"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>平台方式：1季付，2半年付，3年付：</label></td>
					<td class="width-35">
						<form:input path="platformPayType" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">还款期数：3,4：</label></td>
					<td class="width-35">
						<form:input path="payTerm" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">台账号：</label></td>
					<td class="width-35">
						<form:input path="changeNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">基础费率：</label></td>
					<td class="width-35">
						<form:input path="rate" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">月租金：</label></td>
					<td class="width-35">
						<form:input path="cash" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>租户姓名：</label></td>
					<td class="width-35">
						<form:input path="tenancyName" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>租户手机号：</label></td>
					<td class="width-35">
						<form:input path="tenancyMobile" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">起租日：</label></td>
					<td class="width-35">
						<form:input path="startTime" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>到期时间：</label></td>
					<td class="width-35">
						<form:input path="endTime" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">服务费承担方：1租客，2公寓：</label></td>
					<td class="width-35">
						<form:input path="feeType" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">小区名称：</label></td>
					<td class="width-35">
						<form:input path="houseName" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">门牌号：</label></td>
					<td class="width-35">
						<form:input path="houseCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">房间号：</label></td>
					<td class="width-35">
						<form:input path="roomNum" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">二维码生成地址：</label></td>
					<td class="width-35">
						<form:input path="qrcodeUrl" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">租赁合同图片地址：</label></td>
					<td class="width-35">
						<form:input path="orderUrl" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">业主身份证地址：</label></td>
					<td class="width-35">
						<form:input path="ownerUrl" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">房产证图片地址：</label></td>
					<td class="width-35">
						<form:input path="houseUrl" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">代理合同图片地址：</label></td>
					<td class="width-35">
						<form:input path="agentUrl" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">租户应付总租金：</label></td>
					<td class="width-35">
						<form:input path="siteUserTotalAmt" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">审核日期：</label></td>
					<td class="width-35">
						<input id="checkDate" name="checkDate" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${siteUserOrder.checkDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>