<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>公寓管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			laydate({
	            elem: '#beginTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
			laydate({
	            elem: '#updateTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>公寓管理列表 </h5>
		<div class="ibox-tools">
			<a class="collapse-link">
				<i class="fa fa-chevron-up"></i>
			</a>
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-wrench"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#">选项1</a>
				</li>
				<li><a href="#">选项2</a>
				</li>
			</ul>
			<a class="close-link">
				<i class="fa fa-times"></i>
			</a>
		</div>
	</div>
    
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="siteCompany" action="${ctx}/sitecompany/siteCompany/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>公寓名称：</span>
				<form:input path="name" htmlEscape="false" maxlength="50"  class=" form-control input-sm"/>
			<!-- 
				<span>公寓ID：</span>
					<form:input path="id" htmlEscape="false" maxlength="5"  class=" form-control input-sm"/>
				<span>经纪人ID：</span>
					<form:input path="brokerUserId" htmlEscape="false" maxlength="100"  class=" form-control input-sm"/>
				<span>合作开始时间：</span>
					<input id="beginTime" name="beginTime" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
						value="<fmt:formatDate value="${siteCompany.beginTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
				<span>合作终止时间：</span>
					<input id="updateTime" name="updateTime" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm"
						value="<fmt:formatDate value="${siteCompany.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
		   -->
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="sitecompany:siteCompany:add">
				<table:addRow url="${ctx}/sitecompany/siteCompany/form" title="公寓管理"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="sitecompany:siteCompany:edit">
			    <table:editRow url="${ctx}/sitecompany/siteCompany/form" title="公寓管理" id="contentTable"></table:editRow><!-- 编辑按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="sitecompany:siteCompany:del">
				<table:delRow url="${ctx}/sitecompany/siteCompany/deleteAll" id="contentTable"></table:delRow><!-- 删除按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="sitecompany:siteCompany:import">
				<table:importExcel url="${ctx}/sitecompany/siteCompany/import"></table:importExcel><!-- 导入按钮 -->
			</shiro:hasPermission>
			<shiro:hasPermission name="sitecompany:siteCompany:export">
	       		<table:exportExcel url="${ctx}/sitecompany/siteCompany/export"></table:exportExcel><!-- 导出按钮 -->
	       	</shiro:hasPermission>
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		<div class="pull-right">
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="reset()" ><i class="fa fa-refresh"></i> 重置</button>
		</div>
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th> <input type="checkbox" class="i-checks"></th>
				<th  class="sort-column id">公寓ID</th>
				<th  class="sort-column brokerUserId">经纪人ID</th>
				<th  class="sort-column name">公寓名称</th>
				<th  class="sort-column type">公寓类型</th>
				<th  class="sort-column beginTime">是否签署过框架协议</th>
				<th  class="sort-column opName">中介对接人</th>
				<th  class="sort-column linkName">吉星分期对接人</th>
				<th  class="sort-column status">状态</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="siteCompany">
			<tr>
				<td> <input type="checkbox" id="${siteCompany.id}" class="i-checks"></td>
				<td><a  href="#" onclick="openDialogView('查看公寓管理', '${ctx}/sitecompany/siteCompany/form?id=${siteCompany.id}','800px', '500px')">
					${siteCompany.id}
				</a></td>
				<td>
					${siteCompany.brokerUserId}
				</td>
				<td>
					${siteCompany.name}
				</td>
				<td>
					<c:if test="${siteCompany.type==1}">中介</c:if>
					<c:if test="${siteCompany.type==2}">公寓</c:if>
				</td>
				<td>
					<c:if test="${siteCompany.isSign==0}">未签署</c:if>
					<c:if test="${siteCompany.isSign==1}">已签署</c:if>
				</td>
				<td>
					${siteCompany.opName}
				</td>
				<td>
					${siteCompany.linkName}
				</td>
				<td>
					<c:if test="${siteCompany.status==1}">合作中</c:if>
					<c:if test="${siteCompany.status==2}">合作终止</c:if>
				</td>
				<td>
					<shiro:hasPermission name="sitecompany:siteCompany:edit">
    					<a href="#" onclick="openDialog('修改公寓管理', '${ctx}/sitecompany/siteCompany/form?id=${siteCompany.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
    				<shiro:hasPermission name="sitecompany:siteCompany:del">
						<a href="${ctx}/sitecompany/siteCompany/delete?id=${siteCompany.id}" onclick="return confirmx('确认要删除该公寓管理吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>