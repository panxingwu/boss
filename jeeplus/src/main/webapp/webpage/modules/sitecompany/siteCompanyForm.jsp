<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>公寓管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
					laydate({
			            elem: '#beginTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
					laydate({
			            elem: '#updateTime', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
			            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
			        });
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="siteCompany" action="${ctx}/sitecompany/siteCompany/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">公寓名称：</label></td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">公司名称：</label></td>
					<td class="width-35">
						<form:input path="company" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">营业执照编号：</label></td>
					<td class="width-35">
						<form:input path="licence" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">合作开始时间：</label></td>
					<td class="width-35">
						<input id="beginTime" name="beginTime" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${siteCompany.beginTime}" pattern="yyyy-MM-dd"/>"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">合作终止时间：</label></td>
					<td class="width-35">
						<input id="updateTime" name="updateTime" type="text" maxlength="20" class="laydate-icon form-control layer-date "
							value="<fmt:formatDate value="${siteCompany.updateTime}" pattern="yyyy-MM-dd"/>"/>
					</td>
					<td class="width-15 active"><label class="pull-right">是否签订协议：</label></td>
					<td class="width-35">
						<form:select path="isSign" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('fq_issign')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>公寓类型：</label></td>
					<td class="width-35">
						<form:select path="type" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('fq_company_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right">公寓收款类型：</label></td>
					<td class="width-35">
						<form:select path="accountType" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('fq_account_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">所属银行：</label></td>
					<td class="width-35">
						<form:input path="accountBank" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">分支行：</label></td>
					<td class="width-35">
						<form:input path="branchbank" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">银行卡号：</label></td>
					<td class="width-35">
						<form:input path="accountBankcard" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>公寓管理员用户名：</label></td>
					<td class="width-35">
						<form:input path="opName" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>吉星分期对接人用户名：</label></td>
					<td class="width-35">
						<form:input path="linkName" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">宽限天数：</label></td>
					<td class="width-35">
						<form:input path="graceDay" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">滞纳金费率：</label></td>
					<td class="width-35">
						<form:input path="lateFee" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">服务费承担方：</label></td>
					<td class="width-35">
						<c:if test="${siteCompany.feeReceive==null}">
							<form:input value="1|公寓,2|租户" path="feeReceive" htmlEscape="false"    class="form-control" />
						</c:if>
						<c:if test="${siteCompany.feeReceive!=null}">
							<form:input path="feeReceive" htmlEscape="false"    class="form-control" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">平台尾款付款方式：</label></td>
					<td class="width-35">
						<form:select path="retainagePaytype" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('fq_retainage_paytype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>					
					</td>
					<td class="width-15 active"><label class="pull-right">备注：</label></td>
					<td class="width-35">
						<form:input path="info" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">产品类型：</label></td>
					<td class="width-35">
						<c:if test="${siteCompany.remark==null}">
							<form:input value="1|季付,2|半年付,3|年付" path="remark" htmlEscape="false"    class="form-control" />
						</c:if>
						<c:if test="${siteCompany.remark!=null}">
							<form:input path="remark" htmlEscape="false"    class="form-control" />
						</c:if>	
					</td>
					<td class="width-15 active"><label class="pull-right">季付基础费率：</label></td>
					<td class="width-35">
						<form:input path="remark1" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">季付优惠利率：</label></td>
					<td class="width-35">
						<form:input path="remark2" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">季付违约金利率：</label></td>
					<td class="width-35">
						<form:input path="remark3" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">半年付基础费率：</label></td>
					<td class="width-35">
						<form:input path="remark4" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">半年付优惠利率：</label></td>
					<td class="width-35">
						<form:input path="remark5" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">半年付违约金利率：</label></td>
					<td class="width-35">
						<form:input path="remark6" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">年付基础费率：</label></td>
					<td class="width-35">
						<form:input path="remark7" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">年付优惠利率：</label></td>
					<td class="width-35">
						<form:input path="remark8" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">年付违约金利率：</label></td>
					<td class="width-35">
						<form:input path="remark9" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">状态：</label></td>
					<td class="width-35">
						<form:select path="status" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('fq_company_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>					
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>经纪人ID：</label></td>
					<td class="width-35">
						<form:input path="brokerUserId" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">首付方式：</label></td>
					<td class="width-35">
						<c:if test="${siteCompany.remark==null}">
							<form:input value="1|押一付一,2|押二付一" path="firstPayType" htmlEscape="false"    class="form-control "/>
						</c:if>
						<c:if test="${siteCompany.remark!=null}">
							<form:input path="firstPayType" htmlEscape="false"    class="form-control "/>
						</c:if>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>