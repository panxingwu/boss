package com.jeeplus.modules.sys.utils;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;



public class MD5Utils {

	private static final String	salt	= "X1ct";

	public static String encrypt(String str) {
		String encodePwd = null;
		if (str != null) {
			Md5PasswordEncoder passwdEncoder = new Md5PasswordEncoder();
			encodePwd = passwdEncoder.encodePassword(str, salt);
		}
		return encodePwd;
	}
	
	public static String encrypt(String str, boolean isOldUser) {
		if(isOldUser){
			 XLMd5Utils md5 = new XLMd5Utils();
			 return md5.getMD5ofStr(str);
		}else{
			return encrypt(str);
		}
	}

	public static void main(String[] args) {
		System.out.println(MD5Utils.encrypt("123456", false));
		System.out.println(MD5Utils.encrypt("111111", false));
		System.out.println(MD5Utils.encrypt("123456", false));
		System.out.println(MD5Utils.encrypt("wx211115", true));
		System.out.println("wcw:" + MD5Utils.encrypt("2007144235wdwcw", true));
		System.out.println(MD5Utils.encrypt("lt111111", false));
		String pwd = MD5Utils.encrypt("lt111111",true);
		if(pwd.equals("CDB50BD4E895D878964820DFA50E8701")){
			System.out.println("found pwd");
		}
//		ab8542d041cd5c13c62a86045e42f34b
//		CDB50BD4E895D878964820DFA50E8701
	}
}
