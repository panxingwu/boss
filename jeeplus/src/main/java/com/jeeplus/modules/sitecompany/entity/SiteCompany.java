/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sitecompany.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 公寓管理Entity
 * @author 潘兴武
 * @version 2018-02-01
 */
public class SiteCompany extends DataEntity<SiteCompany> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 公寓名称
	private String company;		// 公司名称
	private String licence;		// 营业执照
	private Date beginTime;		// 合作开始时间
	private Date updateTime;		// 合作终止时间
	private String isSign;		// 是否签订协议：0未，1已签
	private String type;		// 公寓类型1:中介，2公寓
	private String accountType;		// 账户类型：1对公，2对私
	private String accountBank;		// 所属银行
	private String branchbank;		// 分支行
	private String accountBankcard;		// 银行卡号
	private String opName;		// 公寓管理员用户名
	private String linkName;		// 吉星分期对接人用户名
	private String graceDay;		// 宽限天数
	private String lateFee;		// 滞纳金费率
	private String feeReceive;		// 服务费承担方:1公寓，2租户（多选）
	private String retainagePaytype;		// 平台尾款付款方式:1尾款单独付，2尾款合并到上期付
	private String info;		// 备注
	private String remark;		// 产品类型：1季付，2半年付，3年付（多选逗号分割）
	private String remark1;		// 季付基础费率
	private String remark2;		// 季付优惠利率
	private String remark3;		// 季付违约金利率
	private String remark4;		// 半年付基础费率
	private String remark5;		// 半年付优惠利率
	private String remark6;		// 半年付违约金利率
	private String remark7;		// 年付基础费率
	private String remark8;		// 年付优惠利率
	private String remark9;		// 年付违约金利率
	private String status;		// 状态：1合作，0停止
	private String brokerUserId;		// 经纪人ID
	private String firstPayType;		// 首付方式
	
	public SiteCompany() {
		super();
	}

	public SiteCompany(String id){
		super(id);
	}

	@ExcelField(title="公寓名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ExcelField(title="公司名称", align=2, sort=2)
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@ExcelField(title="营业执照", align=2, sort=3)
	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="合作开始时间", align=2, sort=4)
	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="合作终止时间", align=2, sort=5)
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	@ExcelField(title="是否签订协议：0未，1已签", dictType="fq_issign", align=2, sort=6)
	public String getIsSign() {
		return isSign;
	}

	public void setIsSign(String isSign) {
		this.isSign = isSign;
	}
	
	@ExcelField(title="公寓类型1:中介，2公寓", dictType="fq_company_type", align=2, sort=7)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="账户类型：1对公，2对私", dictType="fq_account_type", align=2, sort=8)
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	@ExcelField(title="所属银行", align=2, sort=9)
	public String getAccountBank() {
		return accountBank;
	}

	public void setAccountBank(String accountBank) {
		this.accountBank = accountBank;
	}
	
	@ExcelField(title="分支行", align=2, sort=10)
	public String getBranchbank() {
		return branchbank;
	}

	public void setBranchbank(String branchbank) {
		this.branchbank = branchbank;
	}
	
	@ExcelField(title="银行卡号", align=2, sort=11)
	public String getAccountBankcard() {
		return accountBankcard;
	}

	public void setAccountBankcard(String accountBankcard) {
		this.accountBankcard = accountBankcard;
	}
	
	@ExcelField(title="公寓管理员用户名", align=2, sort=12)
	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName;
	}
	
	@ExcelField(title="吉星分期对接人用户名", align=2, sort=13)
	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
	
	@ExcelField(title="宽限天数", align=2, sort=14)
	public String getGraceDay() {
		return graceDay;
	}

	public void setGraceDay(String graceDay) {
		this.graceDay = graceDay;
	}
	
	@ExcelField(title="滞纳金费率", align=2, sort=15)
	public String getLateFee() {
		return lateFee;
	}

	public void setLateFee(String lateFee) {
		this.lateFee = lateFee;
	}
	
	@ExcelField(title="服务费承担方:1公寓，2租户（多选）", align=2, sort=16)
	public String getFeeReceive() {
		return feeReceive;
	}

	public void setFeeReceive(String feeReceive) {
		this.feeReceive = feeReceive;
	}
	
	@ExcelField(title="平台尾款付款方式:1尾款单独付，2尾款合并到上期付", align=2, sort=17)
	public String getRetainagePaytype() {
		return retainagePaytype;
	}

	public void setRetainagePaytype(String retainagePaytype) {
		this.retainagePaytype = retainagePaytype;
	}
	
	@ExcelField(title="备注", align=2, sort=18)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@ExcelField(title="产品类型：1季付，2半年付，3年付（多选逗号分割）", align=2, sort=19)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@ExcelField(title="季付基础费率", align=2, sort=20)
	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	
	@ExcelField(title="季付优惠利率", align=2, sort=21)
	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
	@ExcelField(title="季付违约金利率", align=2, sort=22)
	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	
	@ExcelField(title="半年付基础费率", align=2, sort=23)
	public String getRemark4() {
		return remark4;
	}

	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}
	
	@ExcelField(title="半年付优惠利率", align=2, sort=24)
	public String getRemark5() {
		return remark5;
	}

	public void setRemark5(String remark5) {
		this.remark5 = remark5;
	}
	
	@ExcelField(title="半年付违约金利率", align=2, sort=25)
	public String getRemark6() {
		return remark6;
	}

	public void setRemark6(String remark6) {
		this.remark6 = remark6;
	}
	
	@ExcelField(title="年付基础费率", align=2, sort=26)
	public String getRemark7() {
		return remark7;
	}

	public void setRemark7(String remark7) {
		this.remark7 = remark7;
	}
	
	@ExcelField(title="年付优惠利率", align=2, sort=27)
	public String getRemark8() {
		return remark8;
	}

	public void setRemark8(String remark8) {
		this.remark8 = remark8;
	}
	
	@ExcelField(title="年付违约金利率", align=2, sort=28)
	public String getRemark9() {
		return remark9;
	}

	public void setRemark9(String remark9) {
		this.remark9 = remark9;
	}
	
	@ExcelField(title="状态：1合作，0停止", align=2, sort=29)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="经纪人ID", align=2, sort=30)
	public String getBrokerUserId() {
		return brokerUserId;
	}

	public void setBrokerUserId(String brokerUserId) {
		this.brokerUserId = brokerUserId;
	}
	
	@ExcelField(title="首付方式", align=2, sort=31)
	public String getFirstPayType() {
		return firstPayType;
	}

	public void setFirstPayType(String firstPayType) {
		this.firstPayType = firstPayType;
	}
	
}