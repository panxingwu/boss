/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sitecompany.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.sitecompany.entity.SiteCompany;
import com.jeeplus.modules.sitecompany.service.SiteCompanyService;

/**
 * 公寓管理Controller
 * @author 潘兴武
 * @version 2018-02-01
 */
@Controller
@RequestMapping(value = "${adminPath}/sitecompany/siteCompany")
public class SiteCompanyController extends BaseController {

	@Autowired
	private SiteCompanyService siteCompanyService;
	
	@ModelAttribute
	public SiteCompany get(@RequestParam(required=false) String id) {
		SiteCompany entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = siteCompanyService.get(id);
		}
		if (entity == null){
			entity = new SiteCompany();
		}
		return entity;
	}
	
	/**
	 * 公寓管理列表页面
	 */
	@RequiresPermissions("sitecompany:siteCompany:list")
	@RequestMapping(value = {"list", ""})
	public String list(SiteCompany siteCompany, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SiteCompany> page = siteCompanyService.findPage(new Page<SiteCompany>(request, response), siteCompany); 
		model.addAttribute("page", page);
		return "modules/sitecompany/siteCompanyList";
	}

	/**
	 * 查看，增加，编辑公寓管理表单页面
	 */
	@RequiresPermissions(value={"sitecompany:siteCompany:view","sitecompany:siteCompany:add","sitecompany:siteCompany:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(SiteCompany siteCompany, Model model) {
		model.addAttribute("siteCompany", siteCompany);
		return "modules/sitecompany/siteCompanyForm";
	}

	/**
	 * 保存公寓管理
	 */
	@RequiresPermissions(value={"sitecompany:siteCompany:add","sitecompany:siteCompany:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(SiteCompany siteCompany, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, siteCompany)){
			return form(siteCompany, model);
		}
		if(!siteCompany.getIsNewRecord()){//编辑表单保存
			SiteCompany t = siteCompanyService.get(siteCompany.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(siteCompany, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			siteCompanyService.save(t);//保存
		}else{//新增表单保存
			siteCompanyService.save(siteCompany);//保存
		}
		addMessage(redirectAttributes, "保存公寓管理成功");
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
	}
	
	/**
	 * 删除公寓管理
	 */
	@RequiresPermissions("sitecompany:siteCompany:del")
	@RequestMapping(value = "delete")
	public String delete(SiteCompany siteCompany, RedirectAttributes redirectAttributes) {
		siteCompanyService.delete(siteCompany);
		addMessage(redirectAttributes, "删除公寓管理成功");
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
	}
	
	/**
	 * 批量删除公寓管理
	 */
	@RequiresPermissions("sitecompany:siteCompany:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			siteCompanyService.delete(siteCompanyService.get(id));
		}
		addMessage(redirectAttributes, "删除公寓管理成功");
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("sitecompany:siteCompany:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(SiteCompany siteCompany, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "公寓管理"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<SiteCompany> page = siteCompanyService.findPage(new Page<SiteCompany>(request, response, -1), siteCompany);
    		new ExportExcel("公寓管理", SiteCompany.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出公寓管理记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("sitecompany:siteCompany:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<SiteCompany> list = ei.getDataList(SiteCompany.class);
			for (SiteCompany siteCompany : list){
				try{
					siteCompanyService.save(siteCompany);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条公寓管理记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条公寓管理记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入公寓管理失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
    }
	
	/**
	 * 下载导入公寓管理数据模板
	 */
	@RequiresPermissions("sitecompany:siteCompany:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "公寓管理数据导入模板.xlsx";
    		List<SiteCompany> list = Lists.newArrayList(); 
    		new ExportExcel("公寓管理数据", SiteCompany.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/sitecompany/siteCompany/?repage";
    }
	
	
	

}