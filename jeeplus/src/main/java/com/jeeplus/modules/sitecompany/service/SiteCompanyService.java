/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sitecompany.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import com.jeeplus.modules.sitecompany.entity.SiteCompany;
import com.jeeplus.modules.sitecompany.dao.SiteCompanyDao;

/**
 * 公寓管理Service
 * @author 潘兴武
 * @version 2018-02-01
 */
@Service
@Transactional(readOnly = true)
public class SiteCompanyService extends CrudService<SiteCompanyDao, SiteCompany> {

	public SiteCompany get(String id) {
		return super.get(id);
	}
	
	public List<SiteCompany> findList(SiteCompany siteCompany) {
		return super.findList(siteCompany);
	}
	
	public Page<SiteCompany> findPage(Page<SiteCompany> page, SiteCompany siteCompany) {
		return super.findPage(page, siteCompany);
	}
	
	@Transactional(readOnly = false)
	public void save(SiteCompany siteCompany) {
		super.save(siteCompany);
	}
	
	@Transactional(readOnly = false)
	public void delete(SiteCompany siteCompany) {
		super.delete(siteCompany);
	}
	
	
	
	
}