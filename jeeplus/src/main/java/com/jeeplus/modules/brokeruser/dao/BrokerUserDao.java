/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.brokeruser.dao;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import com.jeeplus.modules.brokeruser.entity.BrokerUser;

/**
 * 经纪人管理DAO接口
 * @author 潘兴武
 * @version 2018-02-01
 */
@MyBatisDao
public interface BrokerUserDao extends CrudDao<BrokerUser> {

	
}