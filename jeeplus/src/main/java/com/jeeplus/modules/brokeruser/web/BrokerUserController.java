/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.brokeruser.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.MyBeanUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.brokeruser.entity.BrokerUser;
import com.jeeplus.modules.brokeruser.service.BrokerUserService;
import com.jeeplus.modules.sys.utils.MD5Utils;

/**
 * 经纪人管理Controller
 * @author 潘兴武
 * @version 2018-02-01
 */
@Controller
@RequestMapping(value = "${adminPath}/brokeruser/brokerUser")
public class BrokerUserController extends BaseController {

	@Autowired
	private BrokerUserService brokerUserService;
	
	@ModelAttribute
	public BrokerUser get(@RequestParam(required=false) String id) {
		BrokerUser entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = brokerUserService.get(id);
		}
		if (entity == null){
			entity = new BrokerUser();
		}
		return entity;
	}
	
	/**
	 * 经纪人管理列表页面
	 */
	@RequiresPermissions("brokeruser:brokerUser:list")
	@RequestMapping(value = {"list", ""})
	public String list(BrokerUser brokerUser, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BrokerUser> page = brokerUserService.findPage(new Page<BrokerUser>(request, response), brokerUser); 
		model.addAttribute("page", page);
		return "modules/brokeruser/brokerUserList";
	}

	/**
	 * 查看，增加，编辑经纪人管理表单页面
	 */
	@RequiresPermissions(value={"brokeruser:brokerUser:view","brokeruser:brokerUser:add","brokeruser:brokerUser:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(BrokerUser brokerUser, Model model) {
		model.addAttribute("brokerUser", brokerUser);
		return "modules/brokeruser/brokerUserForm";
	}

	/**
	 * 保存经纪人管理
	 */
	@RequiresPermissions(value={"brokeruser:brokerUser:add","brokeruser:brokerUser:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(BrokerUser brokerUser, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, brokerUser)){
			return form(brokerUser, model);
		}
		String password = MD5Utils.encrypt(brokerUser.getPassword());
		brokerUser.setPassword(password);
		if(!brokerUser.getIsNewRecord()){//编辑表单保存
			BrokerUser t = brokerUserService.get(brokerUser.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(brokerUser, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			brokerUserService.save(t);//保存
		}else{//新增表单保存
			brokerUserService.save(brokerUser);//保存
		}
		addMessage(redirectAttributes, "保存经纪人管理成功");
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
	}
	
	/**
	 * 删除经纪人管理
	 */
	@RequiresPermissions("brokeruser:brokerUser:del")
	@RequestMapping(value = "delete")
	public String delete(BrokerUser brokerUser, RedirectAttributes redirectAttributes) {
		brokerUserService.delete(brokerUser);
		addMessage(redirectAttributes, "删除经纪人管理成功");
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
	}
	
	/**
	 * 批量删除经纪人管理
	 */
	@RequiresPermissions("brokeruser:brokerUser:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			brokerUserService.delete(brokerUserService.get(id));
		}
		addMessage(redirectAttributes, "删除经纪人管理成功");
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("brokeruser:brokerUser:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(BrokerUser brokerUser, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "经纪人管理"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<BrokerUser> page = brokerUserService.findPage(new Page<BrokerUser>(request, response, -1), brokerUser);
    		new ExportExcel("经纪人管理", BrokerUser.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出经纪人管理记录失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("brokeruser:brokerUser:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<BrokerUser> list = ei.getDataList(BrokerUser.class);
			for (BrokerUser brokerUser : list){
				try{
					brokerUserService.save(brokerUser);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条经纪人管理记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条经纪人管理记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入经纪人管理失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
    }
	
	/**
	 * 下载导入经纪人管理数据模板
	 */
	@RequiresPermissions("brokeruser:brokerUser:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "经纪人管理数据导入模板.xlsx";
    		List<BrokerUser> list = Lists.newArrayList(); 
    		new ExportExcel("经纪人管理数据", BrokerUser.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/brokeruser/brokerUser/?repage";
    }
	
	
	

}