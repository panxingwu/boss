/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.brokeruser.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import com.jeeplus.modules.brokeruser.entity.BrokerUser;
import com.jeeplus.modules.brokeruser.dao.BrokerUserDao;

/**
 * 经纪人管理Service
 * @author 潘兴武
 * @version 2018-02-01
 */
@Service
@Transactional(readOnly = true)
public class BrokerUserService extends CrudService<BrokerUserDao, BrokerUser> {

	public BrokerUser get(String id) {
		return super.get(id);
	}
	
	public List<BrokerUser> findList(BrokerUser brokerUser) {
		return super.findList(brokerUser);
	}
	
	public Page<BrokerUser> findPage(Page<BrokerUser> page, BrokerUser brokerUser) {
		return super.findPage(page, brokerUser);
	}
	
	@Transactional(readOnly = false)
	public void save(BrokerUser brokerUser) {
		super.save(brokerUser);
	}
	
	@Transactional(readOnly = false)
	public void delete(BrokerUser brokerUser) {
		super.delete(brokerUser);
	}
	
	
	
	
}