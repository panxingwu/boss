/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.siteuserorder.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.common.persistence.Page;
import com.jeeplus.common.service.CrudService;
import com.jeeplus.modules.siteuserorder.entity.SiteUserOrder;
import com.jeeplus.modules.siteuserorder.dao.SiteUserOrderDao;

/**
 * 分期订单管理Service
 * @author 潘兴武
 * @version 2018-02-10
 */
@Service
@Transactional(readOnly = true)
public class SiteUserOrderService extends CrudService<SiteUserOrderDao, SiteUserOrder> {

	public SiteUserOrder get(String id) {
		return super.get(id);
	}
	
	public List<SiteUserOrder> findList(SiteUserOrder siteUserOrder) {
		return super.findList(siteUserOrder);
	}
	
	public Page<SiteUserOrder> findPage(Page<SiteUserOrder> page, SiteUserOrder siteUserOrder) {
		return super.findPage(page, siteUserOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(SiteUserOrder siteUserOrder) {
		super.save(siteUserOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(SiteUserOrder siteUserOrder) {
		super.delete(siteUserOrder);
	}
	
	
	
	
}