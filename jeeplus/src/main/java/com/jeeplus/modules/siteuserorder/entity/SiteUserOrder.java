/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.siteuserorder.entity;

import com.jeeplus.modules.sys.entity.User;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.jeeplus.common.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 分期订单管理Entity
 * @author 潘兴武
 * @version 2018-02-10
 */
public class SiteUserOrder extends DataEntity<SiteUserOrder> {
	
	private static final long serialVersionUID = 1L;
	private String orderNo;		// 租户支付订单号
	private User user;		// 经纪人ID
	private String platformPayType;		// 平台方式：1季付，2半年付，3年付
	private String payTerm;		// 还款期数：3,4
	private String changeNo;		// 台账号
	private String returnType;		// 赎回方式：99赎回到银行卡，10转到活期，21转投7天，22转投
	private String applyDate;		// 申购日期20170726
	private String status;		// 订单状态：-1逻辑删除 0:申请中 1:待修改 2待扫码3:待审核，4审核中，5还款中，6已还清，7已违约，8审核拒绝
	private String lateStatus;		// 逾期状态：1未发起清退，2已发起清退，3清退结算中
	private String refuseInfo;		// 拒绝原因（给用户看）
	private String refuseRemark;		// 拒绝真实原因（经纪人能看到）
	private String rate;		// 基础费率
	private String discountRate;		// 优惠利率
	private String delayRate;		// 违约金费率
	private String cash;		// 月租金
	private String tenancyName;		// 租户姓名
	private String tenancyMobile;		// 租户手机号
	private String tenancyIdcard;		// tenancy_idcard
	private String tenancyType;		// 租住方式：1合租 2整租
	private Date startTime;		// 起租日
	private Date endTime;		// 到期时间
	private String feeType;		// 服务费承担方：1租客，2公寓
	private String firstPaytype;		// 首付方式:1押一付一
	private String houseName;		// 小区名称
	private String houseCode;		// 门牌号
	private String roomNum;		// 房间号
	private String settleDate;		// 结算日期例子  20170706  索引查询
	private String info;		// 备注
	private String qrcodeUrl;		// 二维码生成地址
	private String orderUrl;		// 租赁合同图片地址
	private String ownerUrl;		// 业主身份证地址
	private String houseUrl;		// 房产证图片地址
	private String agentUrl;		// 代理合同图片地址
	private String siteUserTotalAmt;		// 租户应付总租金
	private String platTotalAmt;		// 平台应付总金额
	private Date gmtCreate;		// 保存数据库时间
	private Date gmtModify;		// 最后修改时间
	private Date checkDate;		// 审核日期
	private String lateDays;		// 逾期天数
	private Date repayDate;		// 还款日期
	private Date delayDate;		// 发起违约的日期
	private String cityNo;		// 城市编码
	private String timeOffset;		// 租期跨度
	private String checkUserId;		// 审核人姓名
	private String companName;		// 中介公司
	
	public SiteUserOrder() {
		super();
	}

	public SiteUserOrder(String id){
		super(id);
	}

	@ExcelField(title="租户支付订单号", align=2, sort=1)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="经纪人ID", fieldType=User.class, value="user.name", align=2, sort=2)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@ExcelField(title="平台方式：1季付，2半年付，3年付", align=2, sort=3)
	public String getPlatformPayType() {
		return platformPayType;
	}

	public void setPlatformPayType(String platformPayType) {
		this.platformPayType = platformPayType;
	}
	
	@ExcelField(title="还款期数：3,4", align=2, sort=4)
	public String getPayTerm() {
		return payTerm;
	}

	public void setPayTerm(String payTerm) {
		this.payTerm = payTerm;
	}
	
	@ExcelField(title="台账号", align=2, sort=5)
	public String getChangeNo() {
		return changeNo;
	}

	public void setChangeNo(String changeNo) {
		this.changeNo = changeNo;
	}
	
	@ExcelField(title="赎回方式：99赎回到银行卡，10转到活期，21转投7天，22转投", align=2, sort=6)
	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	@ExcelField(title="申购日期20170726", align=2, sort=7)
	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	
	@ExcelField(title="订单状态：-1逻辑删除 0:申请中 1:待修改 2待扫码3:待审核，4审核中，5还款中，6已还清，7已违约，8审核拒绝", dictType="siteuser_order_status", align=2, sort=8)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="逾期状态：1未发起清退，2已发起清退，3清退结算中", align=2, sort=9)
	public String getLateStatus() {
		return lateStatus;
	}

	public void setLateStatus(String lateStatus) {
		this.lateStatus = lateStatus;
	}
	
	@ExcelField(title="拒绝原因（给用户看）", align=2, sort=10)
	public String getRefuseInfo() {
		return refuseInfo;
	}

	public void setRefuseInfo(String refuseInfo) {
		this.refuseInfo = refuseInfo;
	}
	
	@ExcelField(title="拒绝真实原因（经纪人能看到）", align=2, sort=11)
	public String getRefuseRemark() {
		return refuseRemark;
	}

	public void setRefuseRemark(String refuseRemark) {
		this.refuseRemark = refuseRemark;
	}
	
	@ExcelField(title="基础费率", align=2, sort=12)
	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}
	
	@ExcelField(title="优惠利率", align=2, sort=13)
	public String getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(String discountRate) {
		this.discountRate = discountRate;
	}
	
	@ExcelField(title="违约金费率", align=2, sort=14)
	public String getDelayRate() {
		return delayRate;
	}

	public void setDelayRate(String delayRate) {
		this.delayRate = delayRate;
	}
	
	@ExcelField(title="月租金", align=2, sort=15)
	public String getCash() {
		return cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}
	
	@ExcelField(title="租户姓名", align=2, sort=16)
	public String getTenancyName() {
		return tenancyName;
	}

	public void setTenancyName(String tenancyName) {
		this.tenancyName = tenancyName;
	}
	
	@ExcelField(title="租户手机号", align=2, sort=17)
	public String getTenancyMobile() {
		return tenancyMobile;
	}

	public void setTenancyMobile(String tenancyMobile) {
		this.tenancyMobile = tenancyMobile;
	}
	
	@ExcelField(title="tenancy_idcard", align=2, sort=18)
	public String getTenancyIdcard() {
		return tenancyIdcard;
	}

	public void setTenancyIdcard(String tenancyIdcard) {
		this.tenancyIdcard = tenancyIdcard;
	}
	
	@ExcelField(title="租住方式：1合租 2整租", align=2, sort=19)
	public String getTenancyType() {
		return tenancyType;
	}

	public void setTenancyType(String tenancyType) {
		this.tenancyType = tenancyType;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="起租日", align=2, sort=20)
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="到期时间不能为空")
	@ExcelField(title="到期时间", align=2, sort=21)
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@ExcelField(title="服务费承担方：1租客，2公寓", align=2, sort=22)
	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	
	@ExcelField(title="首付方式:1押一付一", align=2, sort=23)
	public String getFirstPaytype() {
		return firstPaytype;
	}

	public void setFirstPaytype(String firstPaytype) {
		this.firstPaytype = firstPaytype;
	}
	
	@ExcelField(title="小区名称", align=2, sort=24)
	public String getHouseName() {
		return houseName;
	}

	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	
	@ExcelField(title="门牌号", align=2, sort=25)
	public String getHouseCode() {
		return houseCode;
	}

	public void setHouseCode(String houseCode) {
		this.houseCode = houseCode;
	}
	
	@ExcelField(title="房间号", align=2, sort=26)
	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
	@ExcelField(title="结算日期例子  20170706  索引查询", align=2, sort=27)
	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
	@ExcelField(title="备注", align=2, sort=28)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@ExcelField(title="二维码生成地址", align=2, sort=29)
	public String getQrcodeUrl() {
		return qrcodeUrl;
	}

	public void setQrcodeUrl(String qrcodeUrl) {
		this.qrcodeUrl = qrcodeUrl;
	}
	
	@ExcelField(title="租赁合同图片地址", align=2, sort=30)
	public String getOrderUrl() {
		return orderUrl;
	}

	public void setOrderUrl(String orderUrl) {
		this.orderUrl = orderUrl;
	}
	
	@ExcelField(title="业主身份证地址", align=2, sort=31)
	public String getOwnerUrl() {
		return ownerUrl;
	}

	public void setOwnerUrl(String ownerUrl) {
		this.ownerUrl = ownerUrl;
	}
	
	@ExcelField(title="房产证图片地址", align=2, sort=32)
	public String getHouseUrl() {
		return houseUrl;
	}

	public void setHouseUrl(String houseUrl) {
		this.houseUrl = houseUrl;
	}
	
	@ExcelField(title="代理合同图片地址", align=2, sort=33)
	public String getAgentUrl() {
		return agentUrl;
	}

	public void setAgentUrl(String agentUrl) {
		this.agentUrl = agentUrl;
	}
	
	@ExcelField(title="租户应付总租金", align=2, sort=34)
	public String getSiteUserTotalAmt() {
		return siteUserTotalAmt;
	}

	public void setSiteUserTotalAmt(String siteUserTotalAmt) {
		this.siteUserTotalAmt = siteUserTotalAmt;
	}
	
	@ExcelField(title="平台应付总金额", align=2, sort=35)
	public String getPlatTotalAmt() {
		return platTotalAmt;
	}

	public void setPlatTotalAmt(String platTotalAmt) {
		this.platTotalAmt = platTotalAmt;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="保存数据库时间", align=2, sort=36)
	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="最后修改时间", align=2, sort=37)
	public Date getGmtModify() {
		return gmtModify;
	}

	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核日期", align=2, sort=38)
	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	
	@ExcelField(title="逾期天数", align=2, sort=39)
	public String getLateDays() {
		return lateDays;
	}

	public void setLateDays(String lateDays) {
		this.lateDays = lateDays;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="还款日期", align=2, sort=40)
	public Date getRepayDate() {
		return repayDate;
	}

	public void setRepayDate(Date repayDate) {
		this.repayDate = repayDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="发起违约的日期", align=2, sort=41)
	public Date getDelayDate() {
		return delayDate;
	}

	public void setDelayDate(Date delayDate) {
		this.delayDate = delayDate;
	}
	
	@ExcelField(title="城市编码", align=2, sort=42)
	public String getCityNo() {
		return cityNo;
	}

	public void setCityNo(String cityNo) {
		this.cityNo = cityNo;
	}
	
	@ExcelField(title="租期跨度", align=2, sort=43)
	public String getTimeOffset() {
		return timeOffset;
	}

	public void setTimeOffset(String timeOffset) {
		this.timeOffset = timeOffset;
	}
	
	@ExcelField(title="审核人姓名", align=2, sort=44)
	public String getCheckUserId() {
		return checkUserId;
	}

	public void setCheckUserId(String checkUserId) {
		this.checkUserId = checkUserId;
	}
	
	@ExcelField(title="中介公司", align=2, sort=45)
	public String getCompanName() {
		return companName;
	}

	public void setCompanName(String companName) {
		this.companName = companName;
	}
	
}