/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.siteuserorder.dao;

import com.jeeplus.common.persistence.CrudDao;
import com.jeeplus.common.persistence.annotation.MyBatisDao;
import com.jeeplus.modules.siteuserorder.entity.SiteUserOrder;

/**
 * 分期订单管理DAO接口
 * @author 潘兴武
 * @version 2018-02-10
 */
@MyBatisDao
public interface SiteUserOrderDao extends CrudDao<SiteUserOrder> {

	
}